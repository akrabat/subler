//
//  PopUpCellView.swift
//  Subler
//
//  Created by Damiano Galassi on 20/10/2017.
//

import Foundation

class PopUpCellView : NSTableCellView {
    @IBOutlet var popUpButton: NSPopUpButton!
}
