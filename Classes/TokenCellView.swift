//
//  TokenCellView.swift
//  Subler
//
//  Created by Damiano Galassi on 06/03/2018.
//

import Cocoa

class TokenCellView : NSTableCellView {
    @IBOutlet var tokenView: NSTokenField!
}
